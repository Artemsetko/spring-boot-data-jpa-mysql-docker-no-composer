# spring-boot-data-jpa-mysql-docker-no-composer

## **Creating** the User
```
POST  /api/users
```

## **Retrieving** all the users
```
GET  /api/users
```

## **Retrieving** a specific User/Resource
```
GET  /api/users/{user_id}
```

## **Updating** a Specific  User/Resource
```
PUT  /api/users/{user_id}
```

## **Removing** a Specific User/Resource
```
DELETE  /api/users/{user_id}
```
