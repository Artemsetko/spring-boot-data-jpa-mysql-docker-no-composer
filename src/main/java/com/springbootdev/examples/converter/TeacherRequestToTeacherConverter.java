package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.TeacherRequest;
import com.springbootdev.examples.model.Teacher;
import com.springbootdev.examples.service.api.CathedraService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TeacherRequestToTeacherConverter implements Converter<TeacherRequest, Teacher> {

    private CathedraService cathedraService;

    public TeacherRequestToTeacherConverter(CathedraService cathedraService) {
        this.cathedraService = cathedraService;
    }

    @Override
    public Teacher convert(TeacherRequest teacherRequest) {
        Teacher teacher = new Teacher();
        teacher.setId(teacherRequest.getId());
        teacher.setFirstName(teacherRequest.getFirstName());
        teacher.setLastName(teacherRequest.getLastName());
        teacher.setCountry(teacherRequest.getCountry());
        teacher.setAddress(teacherRequest.getAddress());
        teacher.setBirthdayDate(teacherRequest.getBirthdayDate());
        teacher.setPassportNumber(teacherRequest.getPassportNumber());
        teacher.setCathedra(cathedraService.getCathedraById(teacherRequest.getCathedraId()));
        return teacher;
    }
}
