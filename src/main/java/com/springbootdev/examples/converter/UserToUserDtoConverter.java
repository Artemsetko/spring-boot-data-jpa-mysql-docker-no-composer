package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.UserDTO;
import com.springbootdev.examples.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDtoConverter implements Converter<User, UserDTO> {

    @Override
    public UserDTO convert(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(), user.getCountry(),
                user.getPassportNumber(), user.getAddress(), user.getBirthdayDate());
    }
}

