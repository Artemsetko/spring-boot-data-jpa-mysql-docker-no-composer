package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.SpecialityRequest;
import com.springbootdev.examples.model.Faculty;
import com.springbootdev.examples.model.Speciality;
import com.springbootdev.examples.service.api.FacultyService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SpecialityRequestToSpecialityConverter implements Converter<SpecialityRequest, Speciality> {
    private FacultyService facultyService;

    public SpecialityRequestToSpecialityConverter(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @Override
    public Speciality convert(SpecialityRequest specialityRequest) {
        Faculty faculty = facultyService.getFacultyById(specialityRequest.getFacultyId());
        Speciality speciality = new Speciality(specialityRequest.getId(), specialityRequest.getName(), faculty);
        return speciality;
    }
}
