package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.GroupRequest;
import com.springbootdev.examples.model.Group;
import com.springbootdev.examples.service.api.SpecialityService;
import com.springbootdev.examples.service.api.TeacherService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class GroupRequestToGroupConverter implements Converter<GroupRequest, Group> {

    private TeacherService teacherService;
    private SpecialityService specialityService;

    public GroupRequestToGroupConverter(TeacherService teacherService, SpecialityService specialityService) {
        this.teacherService = teacherService;
        this.specialityService = specialityService;
    }

    @Override
    public Group convert(GroupRequest groupRequest) {
        Group group = new Group();
        group.setId(groupRequest.getId());
        group.setYearEntered(groupRequest.getYearEntered());
        group.setGroupNumber(groupRequest.getGroupNumber());
        group.setCurator(teacherService.getTeacherById(groupRequest.getCuratorId()));
        group.setSpeciality(specialityService.getSpecialityById(groupRequest.getSpecialityId()));
        return group;
    }
}
