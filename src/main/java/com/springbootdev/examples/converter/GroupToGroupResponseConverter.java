package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.GroupResponse;
import com.springbootdev.examples.model.Group;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class GroupToGroupResponseConverter implements Converter<Group, GroupResponse> {
    @Override
    public GroupResponse convert(Group group) {
        GroupResponse groupResponse = new GroupResponse();
        groupResponse.setId(group.getId());
        groupResponse.setYearEntered(group.getYearEntered());
        groupResponse.setGroupNumber(group.getGroupNumber());
        groupResponse.setCurator(group.getCurator());
        groupResponse.setSpeciality(group.getSpeciality());
        return groupResponse;
    }
}
