package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.FacultyDTO;
import com.springbootdev.examples.model.Faculty;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FacultyDtoToFacultyConverter implements Converter<FacultyDTO, Faculty> {

    @Override
    public Faculty convert(FacultyDTO facultyDTO) {
        return new Faculty(facultyDTO.getId(), facultyDTO.getName(), facultyDTO.getAddress());
    }
}
