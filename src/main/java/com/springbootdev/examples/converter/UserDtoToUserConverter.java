package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.UserDTO;
import com.springbootdev.examples.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDtoToUserConverter implements Converter<UserDTO, User> {

    @Override
    public User convert(UserDTO userDTO) {
        if (userDTO == null) return null;
        return new User(userDTO.getId(), userDTO.getFirstName(), userDTO.getLastName(), userDTO.getCountry(),
                userDTO.getPassportNumber(), userDTO.getAddress(), userDTO.getBirthdayDate());
    }
}
