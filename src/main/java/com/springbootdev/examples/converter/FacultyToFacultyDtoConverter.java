package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.FacultyDTO;
import com.springbootdev.examples.model.Faculty;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FacultyToFacultyDtoConverter implements Converter<Faculty, FacultyDTO> {
    @Override
    public FacultyDTO convert(Faculty faculty) {
        return new FacultyDTO(faculty.getId(), faculty.getName(), faculty.getAddress());
    }
}
