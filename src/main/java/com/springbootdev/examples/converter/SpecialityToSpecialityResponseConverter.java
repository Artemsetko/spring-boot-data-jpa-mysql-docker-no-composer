package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.SpecialityResponse;
import com.springbootdev.examples.model.Speciality;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SpecialityToSpecialityResponseConverter implements Converter<Speciality, SpecialityResponse> {
    @Override
    public SpecialityResponse convert(Speciality speciality) {
        return new SpecialityResponse(speciality.getId(), speciality.getName(), speciality.getFaculty());
    }
}
