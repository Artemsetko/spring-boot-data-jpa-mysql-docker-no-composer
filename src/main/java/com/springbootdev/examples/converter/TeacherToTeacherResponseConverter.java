package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.TeacherResponse;
import com.springbootdev.examples.model.Teacher;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TeacherToTeacherResponseConverter implements Converter<Teacher, TeacherResponse> {
    @Override
    public TeacherResponse convert(Teacher teacher) {
        TeacherResponse teacherResponse = new TeacherResponse();
        teacherResponse.setId(teacher.getId());
        teacherResponse.setFirstName(teacher.getFirstName());
        teacherResponse.setLastName(teacher.getLastName());
        teacherResponse.setCountry(teacher.getCountry());
        teacherResponse.setAddress(teacher.getAddress());
        teacherResponse.setBirthdayDate(teacher.getBirthdayDate());
        teacherResponse.setPassportNumber(teacher.getPassportNumber());
        teacherResponse.setCathedra(teacher.getCathedra());
        return teacherResponse;
    }
}
