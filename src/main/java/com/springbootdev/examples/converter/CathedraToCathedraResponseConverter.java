package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.CathedraResponse;
import com.springbootdev.examples.model.Cathedra;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CathedraToCathedraResponseConverter implements Converter<Cathedra, CathedraResponse> {
    @Override
    public CathedraResponse convert(Cathedra cathedra) {
        return new CathedraResponse(cathedra.getId(), cathedra.getName(), cathedra.getFaculty());
    }
}
