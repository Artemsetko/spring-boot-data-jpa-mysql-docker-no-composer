package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.StudentResponse;
import com.springbootdev.examples.model.Student;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StudentToStudentResponseConverter implements Converter<Student, StudentResponse> {
    @Override
    public StudentResponse convert(Student student) {
        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setId(student.getId());
        studentResponse.setGroup(student.getGroup());
        studentResponse.setAddress(student.getAddress());
        studentResponse.setBirthdayDate(student.getBirthdayDate());
        studentResponse.setCountry(student.getCountry());
        studentResponse.setFirstName(student.getFirstName());
        studentResponse.setLastName(student.getLastName());
        studentResponse.setPassportNumber(student.getPassportNumber());
        return studentResponse;
    }
}
