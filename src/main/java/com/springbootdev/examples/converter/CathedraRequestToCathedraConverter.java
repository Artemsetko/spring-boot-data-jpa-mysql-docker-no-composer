package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.CathedraRequest;
import com.springbootdev.examples.model.Cathedra;
import com.springbootdev.examples.model.Faculty;
import com.springbootdev.examples.service.api.FacultyService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CathedraRequestToCathedraConverter implements Converter<CathedraRequest, Cathedra> {
    private FacultyService facultyService;

    public CathedraRequestToCathedraConverter(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @Override
    public Cathedra convert(CathedraRequest cathedraRequest) {
        Faculty faculty = facultyService.getFacultyById(cathedraRequest.getFacultyId());
        Cathedra cathedra = new Cathedra(cathedraRequest.getName(), faculty);
        if (cathedraRequest.getId() != null) cathedra.setId(cathedraRequest.getId());
        return cathedra;
    }
}
