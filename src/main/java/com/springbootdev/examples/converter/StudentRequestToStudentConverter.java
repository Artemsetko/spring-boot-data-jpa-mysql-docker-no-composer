package com.springbootdev.examples.converter;

import com.springbootdev.examples.dto.StudentRequest;
import com.springbootdev.examples.model.Student;
import com.springbootdev.examples.service.api.GroupService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StudentRequestToStudentConverter implements Converter<StudentRequest, Student> {
    
    private GroupService groupService;

    public StudentRequestToStudentConverter(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public Student convert(StudentRequest studentRequest) {
        Student student = new Student();
        student.setId(studentRequest.getId());
        student.setGroup(groupService.getGroupById(studentRequest.getGroupId()));
        student.setAddress(studentRequest.getAddress());
        student.setBirthdayDate(studentRequest.getBirthdayDate());
        student.setCountry(studentRequest.getCountry());
        student.setFirstName(studentRequest.getFirstName());
        student.setLastName(studentRequest.getLastName());
        student.setPassportNumber(studentRequest.getPassportNumber());
        return student;
    }
}
