package com.springbootdev.examples.bootstrap;

import com.springbootdev.examples.model.*;
import com.springbootdev.examples.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class Bootstrap implements CommandLineRunner {

    private FacultyRepository facultyRepository;
    private SpecialityRepository specialityRepository;
    private CathedraRepository cathedraRepository;
    private GroupRepository groupRepository;
    private StudentRepository studentRepository;
    private TeacherRepository teacherRepository;
    private UserRepository userRepository;

    public Bootstrap(FacultyRepository facultyRepository,
                     SpecialityRepository specialityRepository,
                     CathedraRepository cathedraRepository,
                     GroupRepository groupRepository,
                     StudentRepository studentRepository,
                     TeacherRepository teacherRepository,
                     UserRepository userRepository) {
        this.facultyRepository = facultyRepository;
        this.specialityRepository = specialityRepository;
        this.cathedraRepository = cathedraRepository;
        this.groupRepository = groupRepository;
        this.studentRepository = studentRepository;
        this.teacherRepository = teacherRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        //BOOTSTRAP FACULTIES
        Faculty faculty1 = saveFaculty("Fakultet prikladnoy matematiki", "Karla Marksa, 4");
        Faculty faculty2 = saveFaculty("Mekhaniko-matematicheskiy fakultet", "Kazakova, 18");
        Faculty faculty3 = saveFaculty("Fakultet mezhdunarodnoy ekonomiki","Nauchnaya, 13");
        Faculty faculty4 = saveFaculty("Fakultet psikhologii","Yavornitskogo, 36");
        Faculty faculty5 = saveFaculty("Yuridicheskiy fakultet","Nauchnaya, 13");
        Faculty faculty6 = saveFaculty("Istoricheskiy fakultet","Gagarina, 72");
        Faculty faculty7 = saveFaculty("Ekonomicheskiy fakultet","Yavornitskogo, 35");

        //BOOTSTRAP SPECIALITIES
        Speciality speciality1 = saveSpeciality("Sistemniy analiz", faculty1);
        Speciality speciality2 = saveSpeciality("Programnaya ingeneriya", faculty1);

        //BOOTSTRAP CATHEDRAS
        Cathedra cathedra1 = saveCathedra("Kafedra vychislitelnoy matematiki", faculty1);
        Cathedra cathedra2 = saveCathedra("Kafedra kompyuternykh tekhnologiy", faculty1);

        //BOOTSTRAP TEACHERS
        Teacher teacher1 = saveTeacher("Alexandr Alexandrovich", "Kuzenkov", cathedra1,
                "Pravdy, 21", "Ukraine", "AP 345678",
                LocalDate.of(1970, 3, 1));
        Teacher teacher2 = saveTeacher("Nadezhda Karlovna", "Segeda", cathedra1,
                "Pobedy, 78", "Ukraine", "OH 123456",
                LocalDate.of(1980, 6, 11));

        //BOOTSTRAP GROUPS
        Group group = saveGroup(teacher1, speciality2, 2010, 2);

        //BOOTSTRAP STUDENTS
        Student student1 = saveStudent("Artem", "Sietko", group, "Yavornitskogo, 21",
                "Ukraine", "AH 498367", LocalDate.of(1989, 8, 26));
        Student student2 = saveStudent("Dmitry", "Nikolaiev", group, "Artema, 44",
                "Ukraine", "AB 999654", LocalDate.of(1992, 5, 17));
        Student student3 = saveStudent("Ivan", "Ivanov", group, "Kirova, 12",
                "Ukraine", "AP 382970", LocalDate.of(1988, 6, 7));


    }

    private Faculty saveFaculty(String name, String address){
        Faculty faculty = new Faculty(name, address);
        return facultyRepository.save(faculty);
    }

    private Speciality saveSpeciality(String name, Faculty faculty){
        Speciality speciality = new Speciality(name, faculty);
        return specialityRepository.save(speciality);
    }

    private Cathedra saveCathedra(String name, Faculty faculty){
        Cathedra cathedra = new Cathedra(name, faculty);
        return cathedraRepository.save(cathedra);
    }

    private Teacher saveTeacher(String firstName, String lastName, Cathedra cathedra, String address, String country,
                                String passportNumber, LocalDate birthdayDate){
        Teacher teacher = new Teacher();
        teacher.setFirstName(firstName);
        teacher.setLastName(lastName);
        teacher.setCathedra(cathedra);
        teacher.setAddress(address);
        teacher.setCountry(country);
        teacher.setPassportNumber(passportNumber);
        teacher.setBirthdayDate(birthdayDate);
        return teacherRepository.save(teacher);
    }

    private Group saveGroup(Teacher curator, Speciality speciality, int yearEntered, int groupNumber){
        Group group = new Group();
        group.setCurator(curator);
        group.setSpeciality(speciality);
        group.setYearEntered(yearEntered);
        group.setGroupNumber(groupNumber);
        return groupRepository.save(group);
    }

    private Student saveStudent(String firstName, String lastName, Group group, String address, String country,
                                String passportNumber, LocalDate birthdayDate){
        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setGroup(group);
        student.setPassportNumber(passportNumber);
        student.setAddress(address);
        student.setCountry(country);
        student.setBirthdayDate(birthdayDate);
        return studentRepository.save(student);
    }

}
