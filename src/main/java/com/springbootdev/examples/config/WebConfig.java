package com.springbootdev.examples.config;


import com.springbootdev.examples.converter.*;
import com.springbootdev.examples.service.api.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private FacultyService facultyService;
    private CathedraService cathedraService;
    private TeacherService teacherService;
    private SpecialityService specialityService;
    private GroupService groupService;

    @Lazy
    public WebConfig(FacultyService facultyService, CathedraService cathedraService,
                     TeacherService teacherService, SpecialityService specialityService,
                     GroupService groupService) {
        this.facultyService = facultyService;
        this.cathedraService = cathedraService;
        this.teacherService = teacherService;
        this.specialityService = specialityService;
        this.groupService = groupService;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(userDtoToUserConverter());
        registry.addConverter(userToUserDtoConverter());
        registry.addConverter(studentRequestToStudentConverter());
        registry.addConverter(studentToStudentResponseConverter());
        registry.addConverter(facultyDtoToFacultyConverter());
        registry.addConverter(facultyToFacultyDtoConverter());
        registry.addConverter(specialityRequestToSpecialityConverter());
        registry.addConverter(specialityToSpecialityResponseConverter());
        registry.addConverter(cathedraRequestToCathedraConverter());
        registry.addConverter(cathedraToCathedraResponseConverter());
        registry.addConverter(teacherRequestToTeacherConverter());
        registry.addConverter(teacherToTeacherResponseConverter());
        registry.addConverter(groupRequestToGroupConverter());
        registry.addConverter(groupToGroupResponseConverter());
    }


    @Bean
    public UserToUserDtoConverter userToUserDtoConverter() {
        return new UserToUserDtoConverter();
    }

    @Bean
    public UserDtoToUserConverter userDtoToUserConverter() {
        return new UserDtoToUserConverter();
    }


    @Bean
    public StudentRequestToStudentConverter studentRequestToStudentConverter(){
        return new StudentRequestToStudentConverter(groupService);
    }

    @Bean
    public StudentToStudentResponseConverter studentToStudentResponseConverter(){
        return new StudentToStudentResponseConverter();
    }

    @Bean
    public FacultyToFacultyDtoConverter facultyToFacultyDtoConverter(){
        return new FacultyToFacultyDtoConverter();
    }

    @Bean
    public FacultyDtoToFacultyConverter facultyDtoToFacultyConverter(){
        return new FacultyDtoToFacultyConverter();
    }


    @Bean
    public SpecialityRequestToSpecialityConverter specialityRequestToSpecialityConverter(){
        return new SpecialityRequestToSpecialityConverter(facultyService);
    }

    @Bean
    public SpecialityToSpecialityResponseConverter specialityToSpecialityResponseConverter(){
        return new SpecialityToSpecialityResponseConverter();
    }

    @Bean
    public CathedraRequestToCathedraConverter cathedraRequestToCathedraConverter(){
        return new CathedraRequestToCathedraConverter(facultyService);
    }

    @Bean
    public CathedraToCathedraResponseConverter cathedraToCathedraResponseConverter(){
        return new CathedraToCathedraResponseConverter();
    }

    @Bean
    public TeacherRequestToTeacherConverter teacherRequestToTeacherConverter(){
        return new TeacherRequestToTeacherConverter(cathedraService);
    }

    @Bean
    public TeacherToTeacherResponseConverter teacherToTeacherResponseConverter(){
        return new TeacherToTeacherResponseConverter();
    }

    @Bean
    public GroupRequestToGroupConverter groupRequestToGroupConverter(){
        return new GroupRequestToGroupConverter(teacherService, specialityService);
    }

    @Bean
    public GroupToGroupResponseConverter groupToGroupResponseConverter(){
        return new GroupToGroupResponseConverter();
    }
}
