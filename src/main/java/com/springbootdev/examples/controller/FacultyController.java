package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.FacultyDTO;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Faculty;
import com.springbootdev.examples.service.api.FacultyService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class FacultyController {

    private FacultyService facultyService;
    private ConversionService conversionService;

    public FacultyController(FacultyService facultyService, ConversionService conversionService) {
        this.facultyService = facultyService;
        this.conversionService = conversionService;
    }

    @PostMapping("/faculties")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveFaculty(@RequestBody FacultyDTO faculty) {
        return facultyService.addFaculty(conversionService.convert(faculty, Faculty.class));
    }

    @PutMapping("/faculties/{facultyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateFaculty(@RequestBody FacultyDTO faculty,
                              @PathVariable(name = "facultyId") Long facultyId) {
        if (!facultyService.findFaculty(facultyId).isPresent())
            throw new ResourceNotFoundException("Faculty", "id", facultyId);
        faculty.setId(facultyId);
        facultyService.updateFaculty(conversionService.convert(faculty, Faculty.class));
    }

    @DeleteMapping("/faculties/{facultyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFaculty(@PathVariable(name = "facultyId") Long facultyId) {
        facultyService.deleteFaculty(facultyId);
    }


    @GetMapping("/faculties")
    @ResponseStatus(HttpStatus.OK)
    public List<FacultyDTO> getFaculties(){
        List<Faculty> faculties = facultyService.getAllFaculties();
        List<FacultyDTO> facultyDTOS = faculties.stream()
                .map(faculty -> conversionService.convert(faculty, FacultyDTO.class))
                .collect(Collectors.toList());
        return facultyDTOS;
    }


    @GetMapping("/faculties/{facultyId}")
    @ResponseStatus(HttpStatus.OK)
    public FacultyDTO getFaculty(@PathVariable(name="facultyId") Long facultyId){
        return conversionService.convert(facultyService.getFacultyById(facultyId), FacultyDTO.class);
    }

    @GetMapping("/faculties/name/{facultyName}")
    @ResponseStatus(HttpStatus.OK)
    public FacultyDTO readByFacultyName(@PathVariable("facultyName") String facultyName){
        Faculty faculty = facultyService.getFacultyByName(facultyName);
        return conversionService.convert(faculty, FacultyDTO.class);
    }
}
