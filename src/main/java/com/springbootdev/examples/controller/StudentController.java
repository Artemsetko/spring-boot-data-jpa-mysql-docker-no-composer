package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.StudentRequest;
import com.springbootdev.examples.dto.StudentResponse;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Student;
import com.springbootdev.examples.service.api.StudentService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class StudentController {

    private StudentService studentService;
    private ConversionService conversionService;

    public StudentController(StudentService studentService, ConversionService conversionService) {
        this.studentService = studentService;
        this.conversionService = conversionService;
    }

    @PostMapping("/students")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveStudent(@RequestBody StudentRequest student) {
        return studentService.addStudent(conversionService.convert(student, Student.class));
    }

    @PutMapping("/students/{studentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStudent(@RequestBody StudentRequest student,
                                                @PathVariable(name = "studentId") Long studentId) {
        if (!studentService.findStudent(studentId).isPresent())
            throw new ResourceNotFoundException("Student", "id", studentId);
        student.setId(studentId);
        studentService.updateStudent(conversionService.convert(student, Student.class));
    }

    @DeleteMapping("/students/{studentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable(name = "studentId") Long studentId) {
        studentService.deleteStudent(studentId);
    }

    @GetMapping("/students")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentResponse> getStudents() {
        List<Student> students = studentService.retrieveStudents();
        List<StudentResponse> studentDTOS = students.stream()
                .map(student ->
                        conversionService.convert(student, StudentResponse.class))
                .collect(Collectors.toList());
        return studentDTOS;
    }

    @GetMapping("/students/{studentId}")
    @ResponseStatus(HttpStatus.OK)
    public StudentResponse getStudent(@PathVariable(name = "studentId") Long studentId) {
        return conversionService.convert(studentService.getStudentById(studentId), StudentResponse.class);
    }


    @GetMapping("/students/name/{lastName}")
    @ResponseStatus(HttpStatus.OK)
    public StudentResponse readByLastName(@PathVariable("lastName") String lastName){
        Student student = studentService.getStudentByLastName(lastName);
        return conversionService.convert(student, StudentResponse.class);
    }

}
