package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.TeacherRequest;
import com.springbootdev.examples.dto.TeacherResponse;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Teacher;
import com.springbootdev.examples.service.api.TeacherService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class TeacherController {

    private TeacherService teacherService;
    private ConversionService conversionService;

    public TeacherController(TeacherService teacherService, ConversionService conversionService) {
        this.teacherService = teacherService;
        this.conversionService = conversionService;
    }

    @PostMapping("/teachers")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveTeacher(@RequestBody TeacherRequest teacherRequest){
        return teacherService.createTeacher(conversionService.convert(teacherRequest, Teacher.class));
    }

    @PutMapping("teachers/{teacherId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTeacher(@RequestBody TeacherRequest teacherRequest,
                              @PathVariable(name = "teacherId") Long teacherId) {
        if(!teacherService.findTeacher(teacherId).isPresent())
            throw new ResourceNotFoundException("Teacher", "id", teacherId);
        teacherRequest.setId(teacherId);
        teacherService.updateTeacher(conversionService.convert(teacherRequest, Teacher.class));
    }

    @DeleteMapping("teachers/{teacherId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTeacher(@PathVariable(name = "teacherId") Long teacherId) {
        teacherService.deleteTeacher(teacherId);
    }

    @GetMapping("/teachers")
    @ResponseStatus(HttpStatus.OK)
    public List<TeacherResponse> getTeachers(){
        List<Teacher> teachers = teacherService.getAllTeachers();
        return teachers.stream()
                .map(teacher ->
                        conversionService.convert(teacher, TeacherResponse.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/teachers/{teacherId}")
    @ResponseStatus(HttpStatus.OK)
    public TeacherResponse getTeacher(@PathVariable(name = "teacherId") Long teacherId) {
        return conversionService.convert(teacherService.getTeacherById(teacherId), TeacherResponse.class);
    }

    @GetMapping("teachers/name/{lastName}")
    @ResponseStatus(HttpStatus.OK)
    public TeacherResponse readByLastName(@PathVariable("lastName") String lastName){
        Teacher teacher = teacherService.getTeacherByLastName(lastName);
        return conversionService.convert(teacher, TeacherResponse.class);
    }
}
