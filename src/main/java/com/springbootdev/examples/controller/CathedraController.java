package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.CathedraRequest;
import com.springbootdev.examples.dto.CathedraResponse;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Cathedra;
import com.springbootdev.examples.service.api.CathedraService;
import com.springbootdev.examples.service.api.FacultyService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class CathedraController {
    private CathedraService cathedraService;
    private FacultyService facultyService;
    private ConversionService conversionService;

    public CathedraController(CathedraService cathedraService, FacultyService facultyService,
                              ConversionService conversionService) {
        this.cathedraService = cathedraService;
        this.facultyService = facultyService;
        this.conversionService = conversionService;
    }

    @PostMapping("/cathedras")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveCathedra(@RequestBody CathedraRequest cathedraRequest) {
        return cathedraService.addCathedra(conversionService.convert(cathedraRequest, Cathedra.class));
    }

    @PutMapping("/cathedras/{cathedraId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCathedra(@RequestBody CathedraRequest cathedra,
                                 @PathVariable(name = "cathedraId") Long cathedraId) {
        if (!cathedraService.findCathedra(cathedraId).isPresent())
            throw new ResourceNotFoundException("Cathedra", "id", cathedraId);
        cathedra.setId(cathedraId);
        cathedraService.updateCathedra(conversionService.convert(cathedra, Cathedra.class));
    }

    @DeleteMapping("/cathedras/{cathedraId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFaculty(@PathVariable(name = "cathedraId") Long cathedraId) {
        cathedraService.deleteCathedra(cathedraId);
    }


    @GetMapping("/cathedras")
    @ResponseStatus(HttpStatus.OK)
    public List<CathedraResponse> getCathedras(){
        List<Cathedra> cathedras = cathedraService.getAllCathedras();
        return cathedras.stream()
                .map(cathedra -> conversionService.convert(cathedra, CathedraResponse.class))
                .collect(Collectors.toList());
    }


    @GetMapping("/cathedras/{cathedraId}")
    @ResponseStatus(HttpStatus.OK)
    public CathedraResponse getCathedra(@PathVariable(name="cathedraId") Long cathedraId){
        return conversionService.convert(cathedraService.getCathedraById(cathedraId), CathedraResponse.class);
    }

    @GetMapping("/cathedras/name/{cathedraName}")
    @ResponseStatus(HttpStatus.OK)
    public CathedraResponse readByCathedraName(@PathVariable(name = "cathedraName") String cathedraName){
        Cathedra cathedra = cathedraService.getCathedraByName(cathedraName);
        return conversionService.convert(cathedra, CathedraResponse.class);
    }
}
