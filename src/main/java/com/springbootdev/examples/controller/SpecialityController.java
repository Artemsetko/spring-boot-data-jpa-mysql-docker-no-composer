package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.SpecialityRequest;
import com.springbootdev.examples.dto.SpecialityResponse;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Speciality;
import com.springbootdev.examples.service.api.FacultyService;
import com.springbootdev.examples.service.api.SpecialityService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SpecialityController {
    private SpecialityService specialityService;
    private FacultyService facultyService;
    private ConversionService conversionService;

    public SpecialityController(SpecialityService specialityService, FacultyService facultyService, ConversionService conversionService) {
        this.specialityService = specialityService;
        this.facultyService = facultyService;
        this.conversionService = conversionService;
    }

    @PostMapping("/specialities")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveSpeciality(@RequestBody SpecialityRequest specialityRequest) {
        return specialityService.addSpeciality(conversionService.convert(specialityRequest, Speciality.class));
    }

    @PutMapping("/specialities/{specialityId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSpeciality(@RequestBody SpecialityRequest speciality,
                              @PathVariable(name = "specialityId") Long specialityId) {
        if (!specialityService.findSpeciality(specialityId).isPresent())
            throw new ResourceNotFoundException("Speciality", "id", specialityId);
        speciality.setId(specialityId);
        specialityService.updateSpeciality(conversionService.convert(speciality, Speciality.class));
    }

    @DeleteMapping("/specialities/{specialityId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSpeciality(@PathVariable(name = "specialityId") Long specialityId) {
        specialityService.deleteSpeciality(specialityId);
    }


    @GetMapping("/specialities")
    @ResponseStatus(HttpStatus.OK)
    public List<SpecialityResponse> getSpecialities(){
        List<Speciality> specialities = specialityService.getAllSpecialities();
        List<SpecialityResponse> specialityResponses = specialities.stream()
                .map(speciality -> conversionService.convert(speciality, SpecialityResponse.class))
                .collect(Collectors.toList());
        return specialityResponses;
    }


    @GetMapping("/specialities/{specialityId}")
    @ResponseStatus(HttpStatus.OK)
    public SpecialityResponse getSpeciality(@PathVariable(name="specialityId") Long specialityId){
        return conversionService.convert(specialityService.getSpecialityById(specialityId), SpecialityResponse.class);
    }

    @GetMapping("/specialities/name/{specialtyName}")
    @ResponseStatus(HttpStatus.OK)
    public SpecialityResponse readBySpecilityName(@PathVariable("specialtyName") String specialtyName){
        Speciality speciality = specialityService.getSpecialityByName(specialtyName);
        return conversionService.convert(speciality, SpecialityResponse.class);
    }
}
