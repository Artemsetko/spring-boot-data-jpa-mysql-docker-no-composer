package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.UserDTO;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.User;
import com.springbootdev.examples.service.api.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UserController {

    private UserService userService;
    private ConversionService conversionService;

    public UserController(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }
    @ApiOperation(value = "to create a new user")
    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@RequestBody UserDTO user) {
        return userService.createUser(conversionService.convert(user, User.class));
    }

    @ApiOperation(value = "to get a list of all users")
    @GetMapping("/users")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> readAll() {
        List<User> users = userService.getAllUsers();
        List<UserDTO> userDTOS = users.stream().map(user -> conversionService.convert(user, UserDTO.class))
                .collect(Collectors.toList());
        return userDTOS;
    }

    @ApiOperation(value = "to get user by his id")
    @GetMapping("/users/{user_id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO read(@PathVariable("user_id") Long userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new ResourceNotFoundException("User", "id", userId);
        }
        return conversionService.convert(user, UserDTO.class);
    }

    @ApiOperation(value = "to get user by his last Name")
    @GetMapping("/users/name/{lastName}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO readByLastName(@PathVariable("lastName") String lastName){
        User user = userService.getUserByLastName(lastName);
        if (user == null) {
            throw new ResourceNotFoundException("User", "lastName", lastName);
        }
        return conversionService.convert(user, UserDTO.class);
    }

    @ApiOperation(value = "to update a user by given id")
    @PutMapping("/users/{user_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable("user_id") Long userId, @RequestBody UserDTO user) {
        if (!userService.findUser(userId).isPresent())
            throw new ResourceNotFoundException("User", "id", userId);
        user.setId(userId);
        userService.updateUser(conversionService.convert(user, User.class));
    }

    @ApiOperation(value = "to delete a user")
    @DeleteMapping("/users/{user_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("user_id") Long userId) {
        userService.deleteUser(userId);
    }
}
