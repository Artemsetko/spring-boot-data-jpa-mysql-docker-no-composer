package com.springbootdev.examples.controller;

import com.springbootdev.examples.dto.GroupRequest;
import com.springbootdev.examples.dto.GroupResponse;
import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Group;
import com.springbootdev.examples.service.api.GroupService;
import com.springbootdev.examples.service.api.SpecialityService;
import com.springbootdev.examples.service.api.TeacherService;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class GroupController {

    private TeacherService teacherService;
    private SpecialityService specialityService;
    private GroupService groupService;
    private ConversionService conversionService;

    public GroupController(TeacherService teacherService, SpecialityService specialityService,
                           GroupService groupService, ConversionService conversionService) {
        this.teacherService = teacherService;
        this.specialityService = specialityService;
        this.groupService = groupService;
        this.conversionService = conversionService;
    }

    @PostMapping("/groups")
    @ResponseStatus(HttpStatus.CREATED)
    public Long saveGroup(@RequestBody GroupRequest groupRequest){
        return groupService.addGroup(conversionService.convert(groupRequest, Group.class));
    }

    @PutMapping("/groups/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateGroup(@RequestBody GroupRequest groupRequest,
                            @PathVariable(name = "groupId") Long groupId){
        if(!groupService.findGroup(groupId).isPresent())
            throw new ResourceNotFoundException("Group", "id", groupId);
        groupRequest.setId(groupId);
        groupService.updateGroup(conversionService.convert(groupRequest, Group.class));
    }

    @DeleteMapping("/groups/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGroup(@PathVariable(name = "groupId") Long groupId){
        groupService.deleteGroup(groupId);
    }

    @GetMapping("/groups")
    @ResponseStatus(HttpStatus.OK)
    public List<GroupResponse> getGroups(){
        List<Group> groups = groupService.getAllGroups();
        return groups.stream().map(group -> conversionService.convert(group, GroupResponse.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/groups/{groupId}")
    @ResponseStatus(HttpStatus.OK)
    public GroupResponse getGroup(@PathVariable(name="groupId") Long groupId){
        return conversionService.convert(groupService.getGroupById(groupId), GroupResponse.class);
    }

    @GetMapping("/groups/number/{groupNumber}")
    @ResponseStatus(HttpStatus.OK)
    public GroupResponse readByGroupNumber(@PathVariable(name = "groupNumber") Integer groupNumber){
        Group group = groupService.getGroupByNumber(groupNumber);
        return conversionService.convert(group, GroupResponse.class);
    }
}
