package com.springbootdev.examples.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "cathedras")
public class Cathedra {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "faculty_id", referencedColumnName = "id", nullable = true)
    @JsonManagedReference
    private Faculty faculty;

    @OneToMany(mappedBy = "cathedra", cascade = CascadeType.REMOVE) @LazyCollection(LazyCollectionOption.EXTRA)
    @JsonBackReference
    private List<Teacher> teachers;

    public Cathedra(String name, Faculty faculty) {
        this.name = name;
        this.faculty = faculty;
    }
}
