package com.springbootdev.examples.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "groups")
public class Group {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "curator_id", referencedColumnName = "id", nullable = true)
    private Teacher curator;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "specialities_id", referencedColumnName = "id", nullable = true)
    private Speciality speciality;

    @Column(name = "year_int")
    private int yearEntered;

    @Column(name = "group_num")
    private int groupNumber;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "group", cascade = CascadeType.REMOVE)
    @JsonBackReference
    private List<Student> students;
}
