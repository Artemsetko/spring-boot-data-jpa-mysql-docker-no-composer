package com.springbootdev.examples.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@Table(name = "specialities")
public class Speciality {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;


    @ManyToOne
    @JoinColumn(name = "faculty_id", referencedColumnName = "id", nullable = true)
    @JsonManagedReference
    private Faculty faculty;

    @JsonBackReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "speciality", cascade = CascadeType.REMOVE)
    private List<Group> groups;

    public Speciality(String name) {
        this.name = name;
    }

    public Speciality(String name, Faculty faculty) {
        this.name = name;
        this.faculty = faculty;
    }

    public Speciality(String name, Faculty faculty, List<Group> groups) {
        this.name = name;
        this.faculty = faculty;
        this.groups = groups;
    }

    public Speciality(Long id, String name, Faculty faculty) {
        this.id = id;
        this.name = name;
        this.faculty = faculty;
    }
}
