package com.springbootdev.examples.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "students")
public class Student extends User {
  /*  @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;*/

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "group_id", referencedColumnName = "id", nullable = true)
    private Group group;
}
