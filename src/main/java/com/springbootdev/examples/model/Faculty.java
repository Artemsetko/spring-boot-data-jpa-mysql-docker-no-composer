package com.springbootdev.examples.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@Table(name = "faculties")
public class Faculty {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @JsonBackReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "faculty", cascade = CascadeType.REMOVE)
    private List<Speciality> specialities;

    @JsonBackReference
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "faculty", cascade = CascadeType.REMOVE)
    private List<Cathedra> cathedras;

    public Faculty(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Faculty(Long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
