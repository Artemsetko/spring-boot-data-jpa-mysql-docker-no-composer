package com.springbootdev.examples.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "teachers")
public class Teacher extends User {
    /*  @Id
      @Column(name = "ID")
      @GeneratedValue(strategy = GenerationType.AUTO)
      private Long id;
  */
    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "cathedras_id", referencedColumnName = "id", nullable = true)
    private Cathedra cathedra;
}
