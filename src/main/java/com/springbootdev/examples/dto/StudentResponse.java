package com.springbootdev.examples.dto;


import com.springbootdev.examples.model.Group;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StudentResponse extends UserDTO {
    private Group group;
}
