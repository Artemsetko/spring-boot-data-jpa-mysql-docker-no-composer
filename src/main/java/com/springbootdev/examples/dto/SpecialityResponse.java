package com.springbootdev.examples.dto;

import com.springbootdev.examples.model.Faculty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SpecialityResponse {
    private Long id;
    private String name;
    private Faculty faculty;
}
