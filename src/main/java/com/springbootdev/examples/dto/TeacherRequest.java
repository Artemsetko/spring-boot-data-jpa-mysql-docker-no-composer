package com.springbootdev.examples.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TeacherRequest extends UserDTO {
    private Long cathedraId;
}
