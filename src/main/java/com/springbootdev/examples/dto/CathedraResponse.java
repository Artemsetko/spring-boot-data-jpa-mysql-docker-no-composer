package com.springbootdev.examples.dto;

import com.springbootdev.examples.model.Faculty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CathedraResponse {
    private Long id;
    private String name;
    private Faculty faculty;
}
