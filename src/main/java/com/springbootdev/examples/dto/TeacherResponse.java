package com.springbootdev.examples.dto;

import com.springbootdev.examples.model.Cathedra;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class TeacherResponse extends UserDTO {
    private Cathedra cathedra;
}
