package com.springbootdev.examples.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CathedraRequest {
    private Long id;
    private String name;
    private Long facultyId;
}
