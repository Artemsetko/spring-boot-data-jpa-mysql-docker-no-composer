package com.springbootdev.examples.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class StudentRequest extends UserDTO {
    private Long groupId;
}
