package com.springbootdev.examples.dto;

import com.springbootdev.examples.model.Speciality;
import com.springbootdev.examples.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupResponse {
    private Long id;

    private Teacher curator;

    private Speciality speciality;

    private int yearEntered;

    private int groupNumber;
}
