package com.springbootdev.examples.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupRequest {

    private Long id;

    private Long curatorId;

    private Long specialityId;

    private int yearEntered;

    private int groupNumber;
}
