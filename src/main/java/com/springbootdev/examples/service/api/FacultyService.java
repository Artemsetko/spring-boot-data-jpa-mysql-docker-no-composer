package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Faculty;

import java.util.List;
import java.util.Optional;

public interface FacultyService {

    Optional<Faculty> findFaculty(Long facultyId);

    Long addFaculty(Faculty faculty);

    void updateFaculty(Faculty faculty);

    Faculty getFacultyById(Long facultyId);

    List<Faculty> getAllFaculties();

    void deleteFaculty(Long facultyId);

    Faculty getFacultyByName(String facultyName);
}
