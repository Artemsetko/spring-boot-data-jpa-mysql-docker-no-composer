package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Cathedra;

import java.util.List;
import java.util.Optional;

public interface CathedraService {
    Optional<Cathedra> findCathedra(Long cathedraId);

    Long addCathedra(Cathedra cathedra);

    void updateCathedra(Cathedra cathedra);

    Cathedra getCathedraById(Long cathedraId);

    List<Cathedra> getAllCathedras();

    void deleteCathedra(Long cathedraId);

    Cathedra getCathedraByName(String cathedraName);
}
