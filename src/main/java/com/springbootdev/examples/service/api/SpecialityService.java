package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Speciality;

import java.util.List;
import java.util.Optional;

public interface SpecialityService {
    Optional<Speciality> findSpeciality(Long specialityId);

    Long addSpeciality(Speciality speciality);

    void updateSpeciality(Speciality speciality);

    Speciality getSpecialityById(Long specialityId);

    List<Speciality> getAllSpecialities();

    void deleteSpeciality(Long specialityId);

    Speciality getSpecialityByName(String specialityName);
}
