package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeacherService {
    List<Teacher> getAllTeachers();

    Teacher getTeacherByFirstName(String firstName);

    Teacher getTeacherByLastName(String lastName);

    Teacher getTeacherById(Long id);

    Long createTeacher(Teacher teacher);

    void updateTeacher(Teacher teacher);

    void deleteTeacher(String teachername);

    void deleteTeacher(Long id);

    Optional<Teacher> findTeacher(Long teacherId);
}
