package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Group;

import java.util.List;
import java.util.Optional;

public interface GroupService {
    Optional<Group> findGroup(Long groupId);

    Long addGroup(Group group);

    void updateGroup(Group group);

    Group getGroupById(Long groupId);

    List<Group> getAllGroups();

    void deleteGroup(Long groupId);

    Group getGroupByNumber(int groupNumber);
}
