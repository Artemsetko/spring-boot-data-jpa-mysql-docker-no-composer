package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    User getUserByFirstName(String firstName);

    User getUserByLastName(String lastName);

    User getUserById(Long id);

    Long createUser(User user);

    void updateUser(User user);

    void deleteUser(String username);

    void deleteUser(Long id);

    Optional<User> findUser(Long userId);
}
