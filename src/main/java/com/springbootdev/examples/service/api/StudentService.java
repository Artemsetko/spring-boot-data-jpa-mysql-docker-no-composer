package com.springbootdev.examples.service.api;

import com.springbootdev.examples.model.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {

    Optional<Student> findStudent(Long studentId);

    Long addStudent(Student student);

    void updateStudent(Student student);

    Student getStudentById(Long studentId);

    void deleteStudent(Long studentID);

    Student getStudentByLastName(String lastName);

    List<Student> retrieveStudents();
}
