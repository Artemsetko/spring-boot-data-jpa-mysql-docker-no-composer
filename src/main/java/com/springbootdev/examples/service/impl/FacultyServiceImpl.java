package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Faculty;
import com.springbootdev.examples.repository.FacultyRepository;
import com.springbootdev.examples.service.api.FacultyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FacultyServiceImpl implements FacultyService {

    private FacultyRepository facultyRepository;

    public FacultyServiceImpl(FacultyRepository facultyRepository) {
        this.facultyRepository = facultyRepository;
    }

    @Override
    public Optional<Faculty> findFaculty(Long facultyId) {
        return facultyRepository.findById(facultyId);
    }

    @Override
    public Long addFaculty(Faculty faculty) {
        return facultyRepository.save(faculty).getId();
    }

    @Override
    public void updateFaculty(Faculty faculty) {
        facultyRepository.save(faculty);
    }

    @Override
    public Faculty getFacultyById(Long facultyId) {
        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(() ->
                new ResourceNotFoundException("Faculty", "id", facultyId));
        return faculty;
    }

    @Override
    public List<Faculty> getAllFaculties() {
        List<Faculty> faculties = (List<Faculty>) facultyRepository.findAll();
        return faculties;
    }

    @Override
    public void deleteFaculty(Long facultyId) {
        facultyRepository.deleteById(facultyId);
    }

    @Override
    public Faculty getFacultyByName(String facultyName) {
        Faculty faculty = facultyRepository.findByName(facultyName).orElseThrow(() ->
                new ResourceNotFoundException("Faculty", "name", facultyName));
        return faculty;
    }
}
