package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.model.Teacher;
import com.springbootdev.examples.repository.TeacherRepository;
import com.springbootdev.examples.service.api.TeacherService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImpl implements TeacherService {

    private TeacherRepository teacherRepository;

    public TeacherServiceImpl(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return (List<Teacher>) teacherRepository.findAll();
    }

    @Override
    public Teacher getTeacherByFirstName(String firstName) {
        return teacherRepository.findByFirstName(firstName).orElse(null);
    }

    @Override
    public Teacher getTeacherByLastName(String lastName) {
        return teacherRepository.findByLastName(lastName).orElse(null);
    }

    @Override
    public Teacher getTeacherById(Long id) {
        return teacherRepository.findById(id).orElse(null);
    }

    @Override
    public Long createTeacher(Teacher teacher) {
        return teacherRepository.save(teacher).getId();
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    public void deleteTeacher(String lastName) {
        teacherRepository.delete(teacherRepository.findByLastName(lastName).orElse(null));
    }

    @Override
    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }

    @Override
    public Optional<Teacher> findTeacher(Long teacherId) {
        return teacherRepository.findById(teacherId);
    }
}
