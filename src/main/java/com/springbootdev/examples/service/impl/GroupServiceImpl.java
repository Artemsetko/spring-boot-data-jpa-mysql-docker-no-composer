package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Group;
import com.springbootdev.examples.repository.GroupRepository;
import com.springbootdev.examples.service.api.GroupService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GroupServiceImpl implements GroupService {

    private GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public Optional<Group> findGroup(Long groupId) {
        return groupRepository.findById(groupId);
    }

    @Override
    public Long addGroup(Group group) {
        return groupRepository.save(group).getId();
    }

    @Override
    public void updateGroup(Group group) {
        groupRepository.save(group);
    }

    @Override
    public Group getGroupById(Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(() ->
                new ResourceNotFoundException("Group", "id", groupId));
        return group;
    }

    @Override
    public List<Group> getAllGroups() {
        return (List<Group>) groupRepository.findAll();
    }

    @Override
    public void deleteGroup(Long groupId) {
        groupRepository.deleteById(groupId);
    }

    @Override
    public Group getGroupByNumber(int groupNumber) {
        Group group = groupRepository.findByGroupNumber(groupNumber).orElseThrow(() ->
                new ResourceNotFoundException("Group", "groupNumber", groupNumber));
        return group;
    }
}
