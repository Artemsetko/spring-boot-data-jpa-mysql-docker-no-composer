package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Student;
import com.springbootdev.examples.repository.StudentRepository;
import com.springbootdev.examples.service.api.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Optional<Student> findStudent(Long studentId) {
        return studentRepository.findById(studentId);
    }

    @Override
    public Long addStudent(Student student) {
        return studentRepository.save(student).getId();
    }


    @Override
    public void updateStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public Student getStudentById(Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(() ->
                new ResourceNotFoundException("Student", "id", studentId));
        return student;
    }

    @Override
    public void deleteStudent(Long studentId) {
        studentRepository.deleteById(studentId);
    }

    @Override
    public Student getStudentByLastName(String lastName) {
        Student student = studentRepository.findByLastName(lastName).orElseThrow(() ->
                new ResourceNotFoundException("Student", "lastName", lastName));
        return student;
    }

    @Override
    public List<Student> retrieveStudents() {
        List<Student> students = (List<Student>) studentRepository.findAll();
        return students;
    }


}
