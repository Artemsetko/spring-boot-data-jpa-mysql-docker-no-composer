package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.model.User;
import com.springbootdev.examples.repository.UserRepository;
import com.springbootdev.examples.service.api.UserService;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository, ConversionService conversionService) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByFirstName(String name) {
        return userRepository.findByFirstName(name).orElse(null);
    }

    @Override
    public User getUserByLastName(String name) {
        return userRepository.findByLastName(name).orElse(null);
    }

    @Override
    public User getUserById(Long id) {
        User user = userRepository.findById(id).orElse(null);
        return user;
    }

    @Override
    public Long createUser(User user) {
        User createdUser = userRepository.save(user);
        return createdUser.getId();
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(String lastname) {
        User user = userRepository.findByLastName(lastname).orElse(null);
        userRepository.delete(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> findUser(Long userId) {
        return userRepository.findById(userId);
    }
}
