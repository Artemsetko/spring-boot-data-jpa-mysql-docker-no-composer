package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Speciality;
import com.springbootdev.examples.repository.SpecialityRepository;
import com.springbootdev.examples.service.api.SpecialityService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SpecialityServiceImpl implements SpecialityService {

    private SpecialityRepository specialityRepository;

    public SpecialityServiceImpl(SpecialityRepository specialityRepository) {
        this.specialityRepository = specialityRepository;
    }

    @Override
    public Optional<Speciality> findSpeciality(Long specialityId) {
        return specialityRepository.findById(specialityId);
    }

    @Override
    public Long addSpeciality(Speciality speciality) {
        return specialityRepository.save(speciality).getId();
    }

    @Override
    public void updateSpeciality(Speciality speciality) {
        specialityRepository.save(speciality);
    }

    @Override
    public Speciality getSpecialityById(Long specialityId) {
        Speciality speciality = specialityRepository.findById(specialityId).orElseThrow(() ->
                new ResourceNotFoundException("Speciality", "id", specialityId));
        return speciality;
    }

    @Override
    public List<Speciality> getAllSpecialities() {
        List<Speciality> specialities = (List<Speciality>) specialityRepository.findAll();
        return specialities;
    }

    @Override
    public void deleteSpeciality(Long specialityId) {
        specialityRepository.deleteById(specialityId);
    }

    @Override
    public Speciality getSpecialityByName(String specialityName) {
        Speciality speciality = specialityRepository.findByName(specialityName).orElseThrow(() ->
                new ResourceNotFoundException("Speciality", "name", specialityName));
        return speciality;
    }
}
