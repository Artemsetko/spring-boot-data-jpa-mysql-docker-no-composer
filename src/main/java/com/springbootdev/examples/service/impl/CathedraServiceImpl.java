package com.springbootdev.examples.service.impl;

import com.springbootdev.examples.exception.ResourceNotFoundException;
import com.springbootdev.examples.model.Cathedra;
import com.springbootdev.examples.repository.CathedraRepository;
import com.springbootdev.examples.service.api.CathedraService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CathedraServiceImpl implements CathedraService {

    private CathedraRepository cathedraRepository;

    public CathedraServiceImpl(CathedraRepository cathedraRepository) {
        this.cathedraRepository = cathedraRepository;
    }

    @Override
    public Optional<Cathedra> findCathedra(Long cathedraId) {
        return cathedraRepository.findById(cathedraId);
    }

    @Override
    public Long addCathedra(Cathedra cathedra) {
        return cathedraRepository.save(cathedra).getId();
    }

    @Override
    public void updateCathedra(Cathedra cathedra) {
        cathedraRepository.save(cathedra);
    }

    @Override
    public Cathedra getCathedraById(Long cathedraId) {
        Cathedra cathedra = cathedraRepository.findById(cathedraId).orElseThrow(() ->
                new ResourceNotFoundException("Cathedra", "id", cathedraId));
        return cathedra;
    }

    @Override
    public List<Cathedra> getAllCathedras() {
        List<Cathedra> cathedras = (List<Cathedra>) cathedraRepository.findAll();
        return cathedras;
    }

    @Override
    public void deleteCathedra(Long cathedraId) {
        cathedraRepository.deleteById(cathedraId);
    }

    @Override
    public Cathedra getCathedraByName(String cathedraName) {
        Cathedra cathedra = cathedraRepository.findByName(cathedraName).orElseThrow(() ->
                new ResourceNotFoundException("cathedra", "name", cathedraName));
        return cathedra;
    }
}
