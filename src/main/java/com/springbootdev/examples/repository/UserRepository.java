package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByLastName(String lastName);
    Optional<User> findByFirstName(String firstName);
    Optional<User> findById(Long id);
}
