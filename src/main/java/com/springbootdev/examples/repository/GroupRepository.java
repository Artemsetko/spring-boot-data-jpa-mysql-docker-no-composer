package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.Group;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GroupRepository extends CrudRepository<Group, Long> {
    Optional<Group> findByGroupNumber(int groupNumber);
}
