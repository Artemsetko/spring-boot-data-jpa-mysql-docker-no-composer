package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.Faculty;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FacultyRepository extends CrudRepository<Faculty, Long> {
    Optional<Faculty> findByName(String name);
}
