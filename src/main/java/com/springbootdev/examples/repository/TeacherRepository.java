package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.Teacher;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeacherRepository extends CrudRepository<Teacher, Long> {
    Optional<Teacher> findByFirstName(String firstName);
    Optional<Teacher> findByLastName(String lastName);
}
