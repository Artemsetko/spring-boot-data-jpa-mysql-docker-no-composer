package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Long> {
    Optional<Student> findByFirstName(String firstName);
    Optional<Student> findByLastName(String lastName);
}
