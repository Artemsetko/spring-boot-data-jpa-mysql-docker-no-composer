package com.springbootdev.examples.repository;

import com.springbootdev.examples.model.Cathedra;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CathedraRepository extends CrudRepository<Cathedra, Long> {
    Optional<Cathedra> findByName(String name);
}
